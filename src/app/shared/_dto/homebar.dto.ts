export class HomebarDto {
  name: string;
  color: string;
  image: string;
}
