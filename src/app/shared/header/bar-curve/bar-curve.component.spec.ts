import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarCurveComponent } from './bar-curve.component';

describe('BarCurveComponent', () => {
  let component: BarCurveComponent;
  let fixture: ComponentFixture<BarCurveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarCurveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarCurveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
