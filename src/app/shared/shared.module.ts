import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiBarComponent } from './ui-bar/ui-bar.component';
import { HomebarTxtComponent } from './home-bar/homebar-txt.component';
import { ContainerComponent } from './container/container.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { LogoComponent } from './header/logo/logo.component';
import { BarCurveComponent } from './header/bar-curve/bar-curve.component';
import { HeroComponent } from './header/hero/hero.component';
import { FooterComponent } from './footer/footer.component';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    UiBarComponent,
    HomebarTxtComponent,
    ContainerComponent,
    HeaderComponent,
    LogoComponent,
    BarCurveComponent,
    HeroComponent,
    FooterComponent
  ],
  exports: [
    UiBarComponent,
    HomebarTxtComponent,
    RouterModule,
    HeaderComponent,
    LogoComponent,
    BarCurveComponent,
    HeroComponent,
    FooterComponent,
    HttpClientModule
  ],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule
  ]
})
export class SharedModule { }
