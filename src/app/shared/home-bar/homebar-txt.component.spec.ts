import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomebarTxtComponent } from './homebar-txt.component';

describe('HomeBarComponent', () => {
  let component: HomebarTxtComponent;
  let fixture: ComponentFixture<HomebarTxtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomebarTxtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomebarTxtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
