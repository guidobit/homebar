import { HomebarDto } from '../_dto/homebar.dto';

export interface HomebarInterface extends HomebarDto {
  id: string;
  name: string;
  color: string;
  image: string;
}
