import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { debounceTime, tap } from 'rxjs/operators';
import { HomebarDto } from '../../shared/_dto/homebar.dto';

@Injectable({
  providedIn: 'root'
})
export class HomebarService {

  private static httpOptions = {
    headers: {
      'Content-Type': 'application/json',
    }
  };

  constructor(private httpClient: HttpClient) {
  }

  public getHomebars(page: number = 1, limit: number = 20): Observable<any> {
    const start = page * limit - limit;
    return this.httpClient.get(`/fridges?_start=${start}&_limit=${limit}&_sort=id&_order=desc`);
  }

  public paginate<T>(observable: (page: number) => Observable<T>, pager: BehaviorSubject<number>, manualPaging = true): Observable<T[]> {
    const subject: Subject<T[]> = new Subject<T[]>();
    let data: Array<T> = [];
    observable = observable.bind(this);

    const fetchPage = (pageNumber) => observable(pageNumber).pipe(
      tap(response => {
        if (!Array.isArray(response) || response.length < 1) {
          pager.complete();
          return;
        }

        if (!manualPaging) {
          pager.next(pageNumber + 1);
        }
      })
    );

    pager.pipe(
      debounceTime(50)
    ).subscribe(
      (pageNumber: number) => fetchPage(pageNumber).subscribe((result) => {
        data = data.concat(result);
        subject.next(data);
      }),
      (err) => console.error(err),
      () => {
        subject.complete();
      },
    );

    return subject;
  }

  public addHomebar(homebar: HomebarDto): Observable<any> {
    return this.httpClient.post('/fridges', JSON.stringify(homebar), HomebarService.httpOptions);
  }
}
