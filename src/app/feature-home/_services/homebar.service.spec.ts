import { TestBed } from '@angular/core/testing';

import { HomebarService } from './homebar.service';

describe('HomebarService', () => {
  let service: HomebarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HomebarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
