import { Component, OnInit, TrackByFunction } from '@angular/core';
import { HomebarInterface } from '../../shared/_interfaces/homebar.interface';
import { HomebarService } from '../_services/homebar.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-homebar-collection',
  templateUrl: './homebar-collection.component.html',
  styleUrls: ['./homebar-collection.component.scss']
})
export class HomebarCollectionComponent implements OnInit {

  public pager: BehaviorSubject<number> = new BehaviorSubject<number>(1);
  private currentPage = 1;

  public homebars: HomebarInterface[] = [];
  public trackBy: TrackByFunction<any> = (index, element) => !!element && !!element.id ? element.id : null;

  constructor(private homebarService: HomebarService) { }

  ngOnInit(): void {
    this.homebarService.paginate(this.homebarService.getHomebars, this.pager, true).subscribe(next => {
      this.homebars = next;
    });
    this.pager.subscribe(next => this.currentPage = next);
  }

  public onVisibleTopIndexChange($event: number): void {
    if ($event === 0) {
      return;
    }
    const visibleItems = 6;
    const buffer = 6;
    if ($event + visibleItems + buffer > (this.currentPage * 20)) {
      this.pager.next(this.currentPage + 1);
    }
  }

}
