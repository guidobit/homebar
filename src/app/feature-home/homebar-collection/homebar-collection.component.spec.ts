import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomebarCollectionComponent } from './homebar-collection.component';

describe('HomebarCollectionComponent', () => {
  let component: HomebarCollectionComponent;
  let fixture: ComponentFixture<HomebarCollectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomebarCollectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomebarCollectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
