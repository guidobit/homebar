import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateHomebarFormComponent } from './create-homebar-form.component';

describe('CreateHomebarFormComponent', () => {
  let component: CreateHomebarFormComponent;
  let fixture: ComponentFixture<CreateHomebarFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateHomebarFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateHomebarFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
