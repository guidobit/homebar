import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { HomebarInterface } from '../../shared/_interfaces/homebar.interface';
import { HomebarDto } from '../../shared/_dto/homebar.dto';
import { HomebarService } from '../_services/homebar.service';

@Component({
  selector: 'app-create-homebar-form',
  templateUrl: './create-homebar-form.component.html',
  styleUrls: ['./create-homebar-form.component.scss']
})
export class CreateHomebarFormComponent implements OnInit {
  public showValidity = false;

  constructor(private homebarService: HomebarService) { }

  public form: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.max(100)]),
    color: new FormControl('', [Validators.required, CreateHomebarFormComponent.validColor]),
    image: new FormControl('', [Validators.required, CreateHomebarFormComponent.validPhoto])
  });

  private static validColor(control: AbstractControl): ValidationErrors | null {
    const valid: boolean = !!['black', 'orange', 'red'].find((x) => x === control.value.toLowerCase());
    return valid ? null : {color: 'Invalid value'};
  }

  private static validPhoto(control: AbstractControl): ValidationErrors | null {
    return +control.value > 0 ? null : {image: 'Invalid value'};
  }

  ngOnInit(): void {
  }

  public getControl(name: string): FormControl {
    return this.form.get(name) as FormControl;
  }

  public onSubmit(): void {
    this.form.updateValueAndValidity();
    this.showValidity = true;
    if (this.form.valid) {
      const {name, color, image} = this.form.getRawValue();
      const homeBar: HomebarDto = {
        name,
        color,
        image
      };

      this.homebarService.addHomebar(homeBar).subscribe(next => {
        console.log(next);
      });
    }
  }
}
