import { Pipe, PipeTransform } from '@angular/core';
import { HomebarInterface } from '../../shared/_interfaces/homebar.interface';

@Pipe({
  name: 'transformAvatarUrl'
})
export class TransformAvatarUrlPipe implements PipeTransform {

  public static idToAssetUrl = (id: number) => `./assets/fridge-avatar${id}.png`;

  transform(value: HomebarInterface[], ...args: unknown[]): HomebarInterface[] {
    return value.map(item => {
      let imageId: number = +item.image.match(/(\d+)/)[0];
      if (imageId > 3 || imageId < 1) {
        imageId = 3;
      }
      item.image = TransformAvatarUrlPipe.idToAssetUrl(imageId);
      return item;
    });
  }
}
