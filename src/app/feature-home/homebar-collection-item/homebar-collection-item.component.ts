import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-homebar-collection-item',
  templateUrl: './homebar-collection-item.component.html',
  styleUrls: ['./homebar-collection-item.component.scss']
})
export class HomebarCollectionItemComponent implements OnInit {

  @Input()
  public name: string;

  @Input()
  public color: string;

  @Input()
  public photo: string;

  constructor() { }

  ngOnInit(): void {
  }

}
