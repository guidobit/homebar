import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomebarCollectionItemComponent } from './homebar-collection-item.component';

describe('HomebarComponent', () => {
  let component: HomebarCollectionItemComponent;
  let fixture: ComponentFixture<HomebarCollectionItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomebarCollectionItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomebarCollectionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
