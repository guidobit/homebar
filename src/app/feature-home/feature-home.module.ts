import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { CreateHomebarFormComponent } from './create-homebar-form/create-homebar-form.component';
import { HomebarCollectionComponent } from './homebar-collection/homebar-collection.component';
import { HomebarCollectionItemComponent } from './homebar-collection-item/homebar-collection-item.component';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CdkVirtualScrollViewport, ScrollingModule } from '@angular/cdk/scrolling';
import { TransformAvatarUrlPipe } from './_pipes/transform-avatar-url.pipe';


@NgModule({
  declarations: [
    HomeComponent,
    CreateHomebarFormComponent,
    HomebarCollectionComponent,
    HomebarCollectionItemComponent,
    TransformAvatarUrlPipe,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    ScrollingModule,
  ],
})
export class FeatureHomeModule { }
